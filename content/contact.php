<?php

	if (isset($_GET['action']))
	{
		switch (strtolower($_GET['action']))
		{
			case 'sent':
				$_SESSION['error'] = "Your email has been received!";
				break;
		}
	}

	if (isset($_POST['email']) && isset($_POST['name']) && isset($_POST['message']))
	{
		
		$to = "trentrbtaylor@gmail.com";
		$subject = "Contact";
		$name = $_POST['name'];
		$msg = $_POST['message'];

		$message = "
		<html>
		<head>
		<title>Contact Email</title>
		</head>
		<body>
		<p>You have been contacted!</p>
		<table>
		<tr>
		<th>$name</th>
		<p>$msg</p>
		</table>
		</body>
		</html>";

		$headers = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

		$headers .= 'From: <' . $_POST['email'] . '>' . "\r\n";

		mail($to, $subject, $message, $headers);
		 
		header("Location: contact.php?action=sent");
	
	} else
	{
			$_SESSION['error'] = "Please fill all boxes.";
	}

?>


<?php require('prefabs/head.php'); ?>
	<body class="is-loading">
		<!-- Wrapper -->
		<div id="wrapper">
			<!-- Main -->
			<section id="main">
				<header>
					<h2>SEND A SECURE MESSAGE</h2>
					<form method="post">
						<div class="field">
							<?php echo $_SESSION['error']; ?>
						</div>
						<div class="field">
							<input type="email" name="email" id="email" placeholder="Email">
						</div>	  
						<div class="field">
							<input type="text" name="name" id="name" placeholder="Name">
						</div>
						<div class="field">
							<textarea name="message" id="message" placeholder="What can I help you with"></textarea>
						</div>
						<ul class="actions">
							<li>
								<input type="submit" class="button" value="Send Secure Message">
							</li>
						</ul>
					</form>
				</header>
			</section>
			<footer id="footer">
				<ul class="copyright"/>
			</footer>
		</div>
		<!-- Scripts -->
		<!--[if lte IE 8]><script src="assets/js/respond.min.js"></script><![endif]-->
		<script>
         if ('addEventListener' in window) {
                window.addEventListener('load', function() { document.body.className = document.body.className.replace(/\bis-loading\b/, ''); });
                document.body.className += (navigator.userAgent.match(/(MSIE|rv:11.0)/) ? ' is-ie' : '');
         }
      </script>
	</body>
</html>