<?php
if (isset($_GET['action'])) {
    switch (strtolower($_GET['action'])) {
        case 'login':
            if (isset($_POST['username']) && isset($_POST['password'])) {
                if (!validateUser($_POST['username'], $_POST['password'])) {
                    $_SESSION['error'] = "Please provide correct credentials.";
                    unset($_GET['action']);
                } else {
					
					header("Location: /trenton/secure-page.php");
					
				}
            } else {
                $_SESSION['error'] = "Please provide credentials.";
                unset($_GET['action']);
            }
            break;
        case 'logout':
            if (loggedIn()) {
                logoutUser();
				$_SESSION['error'] = "You have logged out.";
            } else {
                unset($_GET['action']);
            }
            break;
        case 'requiredlogin':
       	    if (!loggedIn())
       	    {
       	    	$_SESSION['error'] = "You need to login to proceed.";
       	    	unset($_GET['action']);
       	    	break;
       	    }
        case 'registered':
            if (loggedIn()) {
            	$_SESSION['error'] = "You are already logged into an account.";
            	break;
            }
            $_SESSION['error'] = "Your account has been created. Please continue to login.";
            unset($_GET['action']);
            break;
    }
}

if (loggedIn()) {
    $_SESSION['error'] = "You are already logged in.";
} elseif (!isset($_GET['action'])) {
    $sUsername = "";
    if (isset($_POST['username'])) {
        $sUsername = $_POST['username'];
    }
}

$sError = "";
if (isset($_SESSION['error'])) {
    $sError = $_SESSION['error'];
}

?>

<html>
   <?php include('prefabs/head.php'); ?>
   <body class="is-loading">
      <!-- Wrapper -->
      <div id="wrapper">
         <section id="main">
            <header>
               <h2>SECURE LOGIN</h2>
               <form method="post" action="?action=login">
			      <div class="field">
                     <label><?php echo $sError; ?></label>
				  </div>
				  
                  <div class="field">
                     <input type="text" name="username" id="username" placeholder="Username" />
                  </div>
				  <div class="field">
                     <input type="password" name="password" id="password" placeholder="Password" />
                  </div>
                  <ul class="actions">
                     <li><input type="submit" class="button" value="Secure Login"></li>
                  </ul>
               </form>
            </header>
         </section>
         <footer id="footer">
            <ul class="copyright">
            </ul>
         </footer>
      </div>
      <!-- Scripts -->
      <!--[if lte IE 8]><script src="assets/js/respond.min.js"></script><![endif]-->
      <script>
         if ('addEventListener' in window) {
         	window.addEventListener('load', function() { document.body.className = document.body.className.replace(/\bis-loading\b/, ''); });
         	document.body.className += (navigator.userAgent.match(/(MSIE|rv:11\.0)/) ? ' is-ie' : '');
         }
      </script>
   </body>
</html>