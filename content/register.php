 <?php

if (!isset($_GET['action'])) {
    $sUsername = "";
    if (isset($_POST['username'])) {
        $sUsername = $_POST['username'];
    }
}

if (isset($_GET['action'])) {
    switch (strtolower($_GET['action'])) {
        case 'register':
            if (isset($_POST['username']) && isset($_POST['password']) && isset($_POST['email'])) {
				
			bool = loggedIn();
				
			if (isset(bool))
			{
				
				if (!bool)
				{
					header("Location: login.php?action=requiredlogin");
				}
				
                if (createAccount($_POST['username'], $_POST['password'], $_POST['email'])) {
                    $to = $_POST['email'];
                    $subject = "Tripex";
                    $txt = "Hello " . $_POST['username'] . ", you now have access.";
                    $headers = "From: trenton@tripex.net";
                    mail($to, $subject, $txt, $headers);
                    header("Location: login.php?action=registered");
                } else {
                    unset($_GET['action']);
                    $_SESSION['error'] = "Failed to create account";
                }
            } else {
                $_SESSION['error'] = "Please provide credentials.";
                unset($_GET['action']);
            }
			}
            break;
    }
}

$sError = "";
if (isset($_SESSION['error'])) {
    $sError = $_SESSION['error'];
}

?>

<html>
   <body class="is-loading">
      <!-- Wrapper -->
      <div id="wrapper">
         <section id="main">
            <header>
               <h2>SECURE REGISTRATION</h2>
               <form method="post" action="?action=register">
			      <div class="field">
                     <label><?php echo $sError; ?></label>
				  </div>
			<div class="field">
                     <input type="email" name="email" id="email" placeholder="Email" />
                  </div>	  
                  <div class="field">
                     <input type="text" name="username" id="username" placeholder="Username" />
                  </div>
				  <div class="field">
                     <input type="password" name="password" id="password" placeholder="Password" />
                  </div>
                  <ul class="actions">
                     <li><input type="submit" class="button" value="Secure Registration"></li>
                  </ul>
               </form>
            </header>
         </section>
         <footer id="footer">
            <ul class="copyright">
            </ul>
         </footer>
      </div>
      <!-- Scripts -->
      <!--[if lte IE 8]><script src="assets/js/respond.min.js"></script><![endif]-->
      <script>
         if ('addEventListener' in window) {
         	window.addEventListener('load', function() { document.body.className = document.body.className.replace(/\bis-loading\b/, ''); });
         	document.body.className += (navigator.userAgent.match(/(MSIE|rv:11\.0)/) ? ' is-ie' : '');
         }
      </script>
   </body>