<?php

function createAccount($pUsername, $pPassword, $pEmail)
{
    if (!empty($pUsername) && !empty($pPassword) && !empty($pEmail)) {

        $uLen = strlen($pUsername);
        $pLen = strlen($pPassword);
        $eLen = strlen($pEmail);
             
        $eUsername = mysql_real_escape_string($pUsername);
        $sql = "SELECT USERNAME FROM TripexAccounts WHERE USERNAME = '" . $eUsername . "' LIMIT 1";

        $query = mysql_query($sql) or trigger_error("Query Failed: " . mysql_error());

        if ($uLen >= 16 || $uLen <= 3) {
            $_SESSION['error'] = "Usernames must be between 3 and 16 characters.";
        } elseif (preg_match('/[\'^£$%&*()}{@#~?><>,|=+¬-]/', $eUsername)) {
            $_SESSION['error'] = "Usernames cannot contain special characters. (/['^£$%&*()}{@#~?><>,|=+¬-]/)";
        } elseif ($pLen <= 8 || $pLen >= 26) {
            $_SESSION['error'] = "Passwords must be between 8 and 26 characters.";
        } elseif (!strpos($pEmail, '@') !== false) {
            $_SESSION['error'] = "Your email adress is invalid.";
        } else {

            $sql = "INSERT INTO TripexAccounts (`USERNAME`, `PASSWORD`, `EMAIL`) VALUES ('" . $eUsername . "', '" . hashPassword($pPassword, SALT1, SALT2) . "', '" . $pEmail . "');";

            $query = mysql_query($sql) or trigger_error("Query Failed: " . mysql_error());

            if ($query) {
                return true;
            }
        }
    }

    return false;
}

function hashPassword($pPassword, $pSalt1 = "2345#$%@3e", $pSalt2 = "taesa%#@2%^#")
{
    return sha1(md5($pSalt2 . $pPassword . $pSalt1));
}

function loggedIn()
{
    if (isset($_SESSION['loggedin'])
	&& isset($_SESSION['username']))
	{
        return true;
    }
    return false;
}

function logoutUser()
{
    unset($_SESSION['username']);
    unset($_SESSION['loggedin']);

    return true;
}

function validateUser($pUsername, $pPassword)
{

    $sql = "SELECT USERNAME FROM TripexAccounts WHERE USERNAME = '" . mysql_real_escape_string($pUsername) . "' AND PASSWORD = '" . hashPassword($pPassword, SALT1, SALT2) . "'";
    $query = mysql_query($sql) or trigger_error("Query Failed: " . mysql_error());
   
    if (mysql_num_rows($query) == 1) {
    
        $row = mysql_fetch_assoc($query);
        
        $_SESSION['username'] = $row['USERNAME'];
        $_SESSION['loggedin'] = true;

        return true;
    }

    return false;
}

?>