<html>

	<?php include('prefabs/head.php'); ?>

   <body class="is-loading">
      <!-- Wrapper -->
      <div id="wrapper">
         <!-- Main -->
         <section id="main">
            <header>
               <span class="avatar"><img src="images/profilepic.jpg" alt="" /></span>
               <h1>Trenton Taylor</h1>
               <p>Java Developer</p>
            </header>
            <footer>
               <ul class="icons">
                  <li><a href="https://twitter.com/trentrbtaylor_" class="fa-twitter">Twitter</a></li>
				  <li><a href="https://gitlab.com/u/trentr_" class="fa-gitlab">GitLab</a></li>
				  <li><a href="contact.php" class="fa-paper-plane-o">Contact</a></li>
               </ul>
            </footer>
            
           <a href="login/">Login</a> 
            
         </section>
         <footer id="footer">
            <ul class="copyright">
            </ul>
         </footer>
      </div>
      <!-- Scripts -->
      <!--[if lte IE 8]><script src="assets/js/respond.min.js"></script><![endif]-->
      <script>
         if ('addEventListener' in window) {
         	window.addEventListener('load', function() { document.body.className = document.body.className.replace(/\bis-loading\b/, ''); });
         	document.body.className += (navigator.userAgent.match(/(MSIE|rv:11\.0)/) ? ' is-ie' : '');
         }
      </script>
   </body>
</html>