<?php require('prefabs/head.php'); 

	if (!loggedIn())
	{
		header("Location: login?action=requiredlogin");
	}

?>
	<body class="is-loading">
		<!-- Wrapper -->
		<div id="wrapper">
			<!-- Main -->
			<section id="main">
				<header>
					<h2>THIS IS A SECURE PAGE</h2>
					<div class="field">				
						<p>Welcome back </p> <?php echo $_SESSION['username']; ?>
					</div>
				</header>
			</section>
			<footer id="footer">
				<ul class="copyright"/>
			</footer>
		</div>
		<!-- Scripts -->
		<!--[if lte IE 8]><script src="assets/js/respond.min.js"></script><![endif]-->
		<script>
         if ('addEventListener' in window) {
                window.addEventListener('load', function() { document.body.className = document.body.className.replace(/\bis-loading\b/, ''); });
                document.body.className += (navigator.userAgent.match(/(MSIE|rv:11.0)/) ? ' is-ie' : '');
         }
      </script>
	</body>
</html>